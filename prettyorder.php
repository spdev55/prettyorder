<?php

if(!defined('_PS_VERSION_'))
{
	exit;
}

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PrettyOrder extends Module
{
	private $html;
	private $category_id = '2';

	public function __construct()
	{
		$this->name = 'prettyorder';
		$this->tab = 'analytics_stats';
		$this->version = '1.0';
		$this->author = 'siteprosto.com';
		$this->ps_versions_compliancy = ['min' => '1.7.1.0', 'max' => _PS_VERSION_];
		$this->need_instance = 0;
		//$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Pretty Order', array(), 'Modules.Prettyorder.Admin');
		$this->description = $this->l('Модуль для распечатки заказов', array(), 'Modules.Prettyorder.Admin');

	}

	public function install()
	{
		if(!parent::install() OR !$this->registerHook('AdminStatsModules'))
		{
			return false;
		}
		return true;
	}
	/**
	 * getContent returns form for configuring category which using in list for download
	 * @return string form
	 */
	public function getContent()
	{
		if(Tools::isSubmit('setCategory'))
		{
			Configuration::updateValue('PRETTYORDER_CAT', Tools::getValue('category'));
			$html .= $this->displayConfirmation($this->l('Настройки обновлены'));
		}

		$html = '';

		$html .= '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post" class="form-horizontal">
					<select name="category">';
						foreach (Category::getAllCategoriesName() as $value)
						{
			$html .= '<option value="'. $value["name"] . '">' . $value["name"] . '</option>';//
						}
		$html .= '</select>
					<input type="submit" name="setCategory" value="'.$this->l('save').'" class="btn btn-default">
				 </form>';
		return $html;
	}

	public function getConfigFieldsValues()
	{

		return array(
			'export_active' => false,
			'export_category' => 'all',
			'export_delimiter' => ',',
			'export_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
			'export_tax' => 'price_tin'
		);
	}


	public function hookAdminStatsModules()
	{
		$category = Configuration::get('PRETTYORDER_CAT');
		$catID = self::getCategoryId($category);
		$res = $this->getOrdersFromDB($catID['id_category']);
		$img = new Image;
		$sp = new Spreadsheet();
		$sp->getProperties()
		->setCreator("")
		->setLastModifiedBy("")
		->setTitle("Office 2007 XLSX Test Document")
		->setSubject("Office 2007 XLSX Test Document")
		->setDescription(
			"Test document for Office 2007 XLSX, generated using PHP classes."
		)
		->setKeywords("office 2007 openxml php")
		->setCategory("Test result file");


		if(Tools::getValue('export'))
		{
			$sp->setActiveSheetIndex(0);
			$sp->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$sp->getActiveSheet()->getColumnDimension('C')->setWidth(50);
			$sp->getActiveSheet()->getColumnDimension('D')->setWidth(50);
			$as = $sp->getActiveSheet(); //getting index of activesheet
			$sp->getActiveSheet()->getDefaultRowDimension()->setRowHeight(120);
			foreach ($res as $key => $value)
			{
				$posA = 'A'.(string)$key;
				$posB = 'B'.(string)$key;
				$posC = 'C'.(string)$key;
				$posD = 'D'.(string)$key;
				$posE = 'E'.(string)$key;

				$sp->setActiveSheetIndex(0)
				->setCellValue($posA, $value["id_order"]);

				$pathImg = self::getLocalImgUrl($value["id_image"]);

				$drawing = new Drawing();
				$drawing->setPath($pathImg);
				$drawing->setHeight(148);
				$drawing->setCoordinates($posB);
				$drawing->setWorksheet($sp->getActiveSheet());

				$sp->setActiveSheetIndex(0)
				->setCellValue($posC, self::getWebImgUrl($value["id_image"]) . ".jpg")
				->setCellValue($posD, $value["product_name"] . ' | '. $value["product_reference"])
				->setCellValue($posE, $value["product_quantity"]);
			}
			//make header for our sheet
			$titles = ['order №', 'imgage', 'url', 'product name', 'count of product'];
			$sp->getActiveSheet()->insertNewRowBefore(1, 2);
			$sp->setActiveSheetIndex(0)
			->setCellValue('A2', $titles[0])
			->setCellValue('B2', $titles[1])
			->setCellValue('C2', $titles[2])
			->setCellValue('D2', $titles[3])
			->setCellValue('E2', $titles[4]);

			$header = 'Content-Disposition: attachment;filename="orderList_' . $category . '.xls"';
			header('Content-Type: application/vnd.ms-excel');
			header($header);
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
			header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header('Pragma: public'); // HTTP/1.0

			$writer = IOFactory::createWriter($sp, 'Xls');
			$writer->save('php://output');
			exit;
		}


		$this->html =
			'<div class="panel-heading">
				' . $this->displayName . ' <i>Категория </i><span class="label label-primary"> ' . $category  .
			'</span><span class="badge">' . $catID['id_category'] . '</span></div><div class="row">
				<div class="col-xs-12">
					<a class="btn btn-default export-csv" name="submitExport" href="'.Tools::safeOutput($_SERVER['REQUEST_URI'].'&export=1').'">
						<i class="icon-cloud-upload"></i> '.$this->trans('Order List Export', array(), 'Modules.Prettyorder.Admin').'
					</a>
				</div>
				<div class="col-xs-12"></div>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>№ заказа</th>
							<th>Картинка</th>
							<th>Наименование</th>
							<th>Кол-во</th>
						</tr>
					</thead><tbody>';
				foreach ($res as $key => $value)
				{
					// $this->html .= '<div>' . $string['id_order'] . ' -- ' . $string['current_state'] . '-> <img src="' . self::getImgUrl($string['id_image']) . '" alt=""></div>' ;
					$this->html .= '<tr><td>' . $value["id_order"] . '</td><td><img src="' . self::getWebImgUrl($value["id_image"]) . '-large_default.jpg" alt="'. $value["product_name"] .'" width="120" height="auto"></td><td>' . $value["product_name"] . ' | '. $value["product_reference"] . '</td><td>' . $value["product_quantity"] . '</td></tr>';
				}
				$this->html .= '</tbody></table></div>';
				$this->html .= '<div class="row">';
				foreach($this->getProd($catID['id_category'], (int)Configuration::get('PS_LANG_DEFAULT')) as $prod)
				{
					$this->html .= /*var_dump($prod["name"]);*/'<div class="col-xs-3">' . $prod["name"] . '</div><div class="col-xs-3">' . /*self::getImgUrl($prod["id_image"])*/ $img->getImgFolder() . '</div><div class="col-xs-3">' . $prod["id_product"] . '</div><div class="col-xs-3">' . $prod["category_name"] .'</div>';
				}
//				$this->html .= '<img src="' . Tools::getAdminImageUrl("24.jpg", $entities = false) . '" alt="test">';
				$this->html .= '</div>';
				//$this->html .= var_dump($this->getProd((int)Configuration::get('PS_LANG_DEFAULT')));
		return $this->html;
	}

	/**
	 * getCategoryId returns int id of category
	 * @param  string $categoryName [description]
	 * @return array               [description]
	 */
	private static function getCategoryId($categoryName = '')
	{
		$sql = new DbQuery();
		$sql->select('*');
		$sql->from('category_lang', 'cl');
		$con = 'cl.name = "' . $categoryName . '"';
		$sql->where('cl.name = "' . $categoryName . '"');
		return Db::getInstance()->getRow($sql);
	}

	private function getOrdersFromDB($cat)
	{
/*		$sql = 'SELECT pi.id_image, od.id_order, od.product_id, od.product_name, cl.name, od.product_quantity, od.product_reference
				FROM lentik.ps_order_detail od
				INNER JOIN lentik.ps_orders o ON o.id_order = od.id_order
				INNER JOIN lentik.ps_image pi ON pi.id_product = od.product_id
				INNER JOIN lentik.ps_category_product cp ON cp.id_product = od.product_id
				INNER JOIN lentik.ps_category_lang cl ON cl.id_category =  cp.id_category
				WHERE o.current_state = 2 AND cp.id_category = ' . $cat;*/
		$sql = new DbQuery();
		$sql->select('pi.id_image, od.id_order, od.product_id, od.product_name, cl.name, od.product_quantity, od.product_reference');
		$sql->from('order_detail', 'od');
		$sql->innerJoin('orders', 'o', 'o.id_order = od.id_order');
		$sql->innerJoin('image', 'pi', 'pi.id_product = od.product_id');
		$sql->innerJoin('category_product', 'cp', 'cp.id_product = od.product_id');
		$sql->innerJoin('category_lang', 'cl', 'cl.id_category = cp.id_category');
		$sql->where('o.current_state = 2 AND cp.id_category = "' . (int)$cat . '"');

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}

	public function setOption($option, $layers = 1)
	{
		$this->option = $option;
	}



	private static function getCover($product_id)
	{
		//$query = 'SELECT i.id_image FROM ' . _DB_PREFIX_ . '_image i WHERE i.id_product = ' . $product_id;
		$query = 'SELECT i.id_image FROM lentik.ps_image i WHERE i.cover = 1 AND i.id_product = ' . $product_id;
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query);
		return $result['id_image'];
	}

	/**
	 * [getLocalImgUrl description]
	 * @param  [type] $idImage [description]
	 * @return [type]          [description]
	 */
	private static function getLocalImgUrl($idImage)
	{
		$imageUrl=_PS_PROD_IMG_DIR_.Image::getImgFolderStatic($idImage).$idImage."-medium_default.jpg";
		return $imageUrl;
	}

	/**
	 * [getWebImgUrl description]
	 * @param  [type] $idImage [description]
	 * @return [type]          [description]
	 */
	private static function getWebImgUrl($idImage)
	{
		//$imageUrl=_PS_PROD_IMG_DIR_.Image::getImgFolderStatic($value['id_image']).$value['id_image'].Image::getImgFolderStatic($idImage).$idImage.".jpg";
		$imageUrl = 'http://'.$_SERVER["SERVER_NAME"].'/img/p/'.Image::getImgFolderStatic($idImage).$idImage;
		return $imageUrl;
	}
	private function getCat()
	{
		$categoryRoot = new Category(Configuration::get('PS_HOME_CATEGORY'),$this->context->language->id,$this->context->shop->id);
		$categoriesHome = $categoryRoot->getSubCategories($this->context->language->id);

		return $categoriesHome;
	}

	private function getProd($id_category, $id_lang)
	{
		//$id_lang = Tools::getValue('export_language');
		//$id_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		/*$products = Product::getProducts($id_lang,
			0,
			10000,
			'id_order',
			'ASC'
		);*/

		$mujer = new Category($id_category, $id_lang);
		//$products = $mujer->getProducts($id_lang,1,100);
		$products = $mujer->getProducts($id_lang, 1, 100);

		return $products;
	}

	public function postProcess()
	{
		$titles = ['#', 'img', 'name', 'count'];
		$delimiter = ',';

		if(Tools::isSubmit('submitExport'))
		{
			set_time_limit(0);
			$fileName = 'products_' . date("Y_m_d_H_i_s") . '.csv';
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header('Content-Description: File Transfer');
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename={$fileName}");
			header("Expires: 0");
			header("Pragma: public");
			echo "\xEF\xBB\xBF";

			$f = fopen(
				'php://output',
				'w'
			);

			fputcsv(
				$f,
				$titles,
				$delimiter,
				'"'
			);
			fclose($f);
			die();
		}

	}

	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}


}
